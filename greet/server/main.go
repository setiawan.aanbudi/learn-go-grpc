package main

import (
	"golang-grpc/greet/proto"
	"google.golang.org/grpc"
	"log"
	"net"
)

type Server struct {
	learn_go_grpc.GreetServiceServer
}

func main() {
	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen on: %v\n", err)
	}

	log.Printf("Listening on %s,\n", "0.0.0.0:50051")

	s := grpc.NewServer()
	learn_go_grpc.RegisterGreetServiceServer(s, &Server{})
	if err = s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v\n", err)
	}
}
