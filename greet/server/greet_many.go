package main

import (
	"fmt"
	learn_go_grpc2 "golang-grpc/greet/proto"
)

func (s *Server) GreetMany(in *learn_go_grpc2.GreetRequest, stream learn_go_grpc2.GreetService_GreetManyServer) error {

	var err error
	for i := 0; i < 10; i++ {
		err = stream.Send(&learn_go_grpc2.GreetResponse{Result: fmt.Sprintf("Hello %v, number %v", in.FirstName, i)})
		if err != nil {
			return err
		}
	}

	return nil
}
