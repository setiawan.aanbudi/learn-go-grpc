package main

import (
	"context"
	"golang-grpc/greet/proto"
	"log"
)

func (s *Server) Greet(ctx context.Context, in *learn_go_grpc.GreetRequest) (*learn_go_grpc.GreetResponse, error) {
	log.Printf("Greet function was invoked with %v\n", in)
	return &learn_go_grpc.GreetResponse{Result: "HELLO " + in.FirstName}, nil
}
