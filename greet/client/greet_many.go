package main

import (
	"context"
	"fmt"
	learn_go_grpc2 "golang-grpc/greet/proto"
	"io"
	"log"
)

func doGreetMany(c learn_go_grpc2.GreetServiceClient) {
	many, err := c.GreetMany(context.Background(), &learn_go_grpc2.GreetRequest{FirstName: "Anisa yahanana"})
	if err != nil {
		return
	}

	for {
		recv, err := many.Recv()

		if err == io.EOF {
			break
		}

		if err != nil {
			log.Printf("error occurs %v: ", err)
		}

		fmt.Printf("Result, %v\n", recv.Result)
	}
}
