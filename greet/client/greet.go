package main

import (
	"context"
	learn_go_grpc2 "golang-grpc/greet/proto"
	"log"
)

func doGreet(c learn_go_grpc2.GreetServiceClient) {
	log.Printf("The function was invoked %v\n", c)

	res, err := c.Greet(context.Background(), &learn_go_grpc2.GreetRequest{FirstName: "AAN"})
	if err != nil {
		log.Fatalf("Could not greet: %v\n", err)
	}

	log.Printf("Greeting: %s\n", res.Result)
}
