package main

import (
	"fmt"
	"golang-grpc/greet/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

var addr = "0.0.0.0:50051"

func main() {
	conn, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("Failed to connect on: %v\n", err)
	}
	defer conn.Close()

	c := learn_go_grpc.NewGreetServiceClient(conn)
	doGreet(c)
	fmt.Println("----------\t ******** \t----------")
	doGreetMany(c)
}
