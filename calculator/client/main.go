package main

import (
	"context"
	"fmt"
	learn_go_grpc2 "golang-grpc/calculator/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"io"
	"log"
)

func main() {
	var addr = "0.0.0.0:50052"
	conn, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("%v", err)
	}
	defer conn.Close()

	log.Printf("COnenct to %v", addr)

	calculator := learn_go_grpc2.NewCalculatorClient(conn)

	doAddCalculate(calculator)
	fmt.Println("---------\t ******** \t---------")
	doPrimeStream(calculator)
}

func doAddCalculate(calculator learn_go_grpc2.CalculatorClient) {
	add, err := calculator.Add(context.Background(), &learn_go_grpc2.CalculatorRequest{
		A: 56,
		B: 2,
	})
	if err != nil {
		log.Printf("Err: %v", err)
		return
	}

	log.Printf("Reuslt: %v", add)
}

func doPrimeStream(calculator learn_go_grpc2.CalculatorClient) {
	stream, err := calculator.PrimeNumberStream(context.Background(), &learn_go_grpc2.PrimeNumberRequest{A: 15})
	if err != nil {
		return
	}

	for {
		receive, err := stream.Recv()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Printf("Failed %v", err)
		}

		fmt.Printf("Prime number %v\n", receive.GetResult())
	}
}
