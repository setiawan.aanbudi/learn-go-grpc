package main

import (
	learn_go_grpc2 "golang-grpc/calculator/proto"
)

func (c *calculatorService) PrimeNumberStream(in *learn_go_grpc2.PrimeNumberRequest, stream learn_go_grpc2.Calculator_PrimeNumberStreamServer) error {

	var i uint64
	for i = 2; i <= in.GetA(); i++ {

		isPrime := true
		var j uint64
		for j = 2; j <= i/2; j++ {
			if i%j == 0 {
				isPrime = false
				break
			}
		}

		if isPrime {
			err := stream.Send(&learn_go_grpc2.CalculatorResponse{Result: i})
			if err != nil {
				return err
			}
		}
	}

	return nil
}
