package main

import (
	"context"
	"golang-grpc/calculator/proto"
)

func (c *calculatorService) Add(ctx context.Context, request *learn_go_grpc.CalculatorRequest) (*learn_go_grpc.CalculatorResponse, error) {

	return &learn_go_grpc.CalculatorResponse{Result: request.A + request.B}, nil
}
