package main

import (
	"golang-grpc/calculator/proto"
	"google.golang.org/grpc"
	"log"
	"net"
)

type calculatorService struct {
	learn_go_grpc.CalculatorServer
}

func main() {
	addr := "0.0.0.0:50052"
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("failed to listen %v\n", err)
	}
	log.Printf("Listen from %v", addr)

	server := grpc.NewServer()
	learn_go_grpc.RegisterCalculatorServer(server, &calculatorService{})

	if err = server.Serve(lis); err != nil {
		log.Fatalf("failed to serve %v\n", err)
	}
}
