proto-greet:
	protoc -I /Users/macbook/Documents/code/belajar/golang/belajar-grpc/unary/greet/proto --go_out=./unary/greet/proto --go_opt=module=gitlab.com/setiawan.aanbudi/learn-go-grpc --go-grpc_out=./unary/greet/proto --go-grpc_opt=module=gitlab.com/setiawan.aanbudi/learn-go-grpc /Users/macbook/Documents/code/belajar/golang/belajar-grpc/unary/greet/proto/greet.proto

proto-calculator:
	protoc -I /Users/macbook/Documents/code/belajar/golang/belajar-grpc/unary/calculator/proto --go_out=./unary/calculator/proto --go_opt=module=gitlab.com/setiawan.aanbudi/learn-go-grpc --go-grpc_out=./unary/calculator/proto --go-grpc_opt=module=gitlab.com/setiawan.aanbudi/learn-go-grpc /Users/macbook/Documents/code/belajar/golang/belajar-grpc/unary/calculator/proto/calculator.proto

proto-err:
	protoc -I ./error-impl/proto --go_out=./error-impl/proto --go_opt=module=gitlab.com/setiawan.aanbudi/learn-go-grpc --go-grpc_out=./error-impl/proto --go-grpc_opt=module=gitlab.com/setiawan.aanbudi/learn-go-grpc ./error-impl/proto/err_impl.proto
