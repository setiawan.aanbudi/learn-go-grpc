package main

import (
	"context"
	"fmt"
	learn_go_grpc "golang-grpc/error-impl/proto"
	"google.golang.org/grpc/status"
	"log"
)

func getname(client learn_go_grpc.ErrorImplClient) {

	name, err := client.GetName(context.Background(), &learn_go_grpc.Request{Name: "aan"})
	if err != nil {
		e, ok := status.FromError(err)
		if ok {
			fmt.Printf("grpc error %v\n", e)
		} else {
			log.Fatalf("Unknown Err: %v\n", err)
		}
	}

	fmt.Println("Response: ", name)
}
