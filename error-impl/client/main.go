package main

import (
	learn_go_grpc "golang-grpc/error-impl/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

func main() {
	addr := "0.0.0.0:50003"

	dial, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {

		log.Fatalf("error %v", err)
	}
	defer dial.Close()

	client := learn_go_grpc.NewErrorImplClient(dial)

	getname(client)

}
