package main

import (
	"context"
	learn_go_grpc2 "golang-grpc/error-impl/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"strings"
)

func (s *server) GetName(ctx context.Context, r *learn_go_grpc2.Request) (*learn_go_grpc2.Response, error) {

	if strings.EqualFold(r.Name, "aan") {
		return nil, status.Errorf(codes.InvalidArgument, "Error")
	}

	return &learn_go_grpc2.Response{FullName: "learn " + r.Name}, nil
}
