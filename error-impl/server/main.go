package main

import (
	learn_go_grpc "golang-grpc/error-impl/proto"
	"google.golang.org/grpc"
	"log"
	"net"
)

type server struct {
	learn_go_grpc.UnimplementedErrorImplServer
}

func main() {
	lis, err := net.Listen("tcp", "0.0.0.0:50003")
	if err != nil {
		log.Fatalf("error when connect %v\n", err)
	}

	s := grpc.NewServer()

	learn_go_grpc.RegisterErrorImplServer(s, &server{})

	if err := s.Serve(lis); err != nil {
		return
	}
}
